const { Given, When, Then } = require("@cucumber/cucumber");
const assert = require("assert");
const request = require("supertest");
const app = require("../app");

let response;

Given("the user has valid login credentials", function () {
  this.credentials = { username: "user", password: "pass" };
});

Given("the user has invalid login credentials", function () {
  this.credentials = { username: "invalidUser", password: "invalidPassword" };
});

When("the user attempts to login", async function () {
  response = await request(app).post("/login").send(this.credentials);
});

Then("the login should be successful", function () {
  assert.equal(response.status, 200);
  assert.equal(response.body.message, "Login successful");
});

Then("the login should fail", function () {
  assert.equal(response.status, 401);
  assert.equal(response.body.message, "Invalid credentials");
});
