Feature: User Login

  Scenario: Successful login with valid credentials
    Given the user has valid login credentials
    When the user attempts to login
    Then the login should be successful

  Scenario: Failed login with invalid credentials
    Given the user has invalid login credentials
    When the user attempts to login
    Then the login should fail
