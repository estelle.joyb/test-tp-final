function calculateBudget(income, expenses) {
  if (!Array.isArray(expenses) || expenses.length == 0)
    throw new Error("Expenses must be an array");
  expenses.find((expense) => {
    if (expense < 0) {
      throw new Error("expense cannot be negative");
    }
    return expense;
  });
  if (typeof income != "number" || income < 0)
    throw new Error("Income must be a positive number");
  const totalExpenses = expenses.reduce((acc, expense) => acc + expense, 0);
  return income - totalExpenses;
}
module.exports = calculateBudget;
