const express = require("express");
const app = express();
app.use(express.json());

app.post("/login", (req, res) => {
  const { username, password } = req.body;
  console.log("Requête reçue :", req.body);
  console.log("username", username, "password", password);
  if (username == "user" && password === "pass") {
    res.status(200).json({ token: "xyz" });
  } else {
    res.status(401).send("Unauthorized");
  }
});
if (require.main == module) {
  app.listen(8080, () => {});
}

module.exports = app;
