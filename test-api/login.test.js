const request = require("supertest");
const app = require("./app.js");
describe("POST /login", function () {
  it("return token as authent is successful", function (done) {
    request(app)
      .post("/login")
      .send({ username: "user", password: "pass" })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        expect(res.body).toEqual({ token: "xyz" });
        done();
      });
  });

  it("return unauthorized error with 401", function (done) {
    request(app)
      .post("/login")
      .send({ username: "null", password: "booooh" })
      .set("Accept", "application/json")
      .expect(401, done);
  });
});
