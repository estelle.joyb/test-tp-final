const { describe } = require("yargs");
const calculateBudget = require("./calculateBudget.js");

//Calcul correct du budget avec des entrées valides.
test("it should throw error as icome given is a string", () => {
  try {
    calculateBudget("nimportkoi", [15, 36]);
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Income must be a positive number");
  }
});
test("it should throw error as expenses OR income are null or undefined", () => {
  try {
    calculateBudget(null, undefined);
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Expenses must be an array");
  }
});

//Gestion des erreurs lorsque les dépenses ne sont pas un tableau.
test("it should throw error as expenses given is not an array", () => {
  try {
    calculateBudget(1500, "notanarray");
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Expenses must be an array");
  }
});

//Comportement avec des tableaux vides et des valeurs négatives
test("it should throw error as expenses given is empty", () => {
  try {
    calculateBudget(1500, []);
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Expenses must be an array");
  }
});
test("it should throw error as expenses given are negatives", () => {
  try {
    calculateBudget(1500, [-1, -555]);
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("expense cannot be negative");
  }
});

test("it should throw error as income given is negative", () => {
  try {
    calculateBudget(-666, [15, 55]);
    expect(true).toBe(false);
  } catch (e) {
    expect(e.message).toBe("Income must be a positive number");
  }
});
